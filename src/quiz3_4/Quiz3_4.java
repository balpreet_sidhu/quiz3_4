package quiz3_4;

public class Quiz3_4 {
	
	public static void main(String[] args)
	{
		// Question 1
		String example = "copyandpastecopyandpastecopyandpastecopyandpastecopyandpastecopyandpastecommitandpushcommitandpushcommitandpushcommitandpushpushcccccommmitttsss";
		int totalCharacters = example.length();
		System.out.println("Total number of characters are: " + totalCharacters);
		
		// Question 2
		String last = example.substring(129);
		System.out.println("The last 15 characters of the given string are: " + last);
		
		// Question 3
		int count=0;
		for(int i=0;i<example.length()-1;i++)
		{
		 if(example.charAt(i)=='c')
		  count++;
		}
		System.out.println("The number of occurrance of character c is:"+count);
		int count1=0;
		for(int j=0;j<example.length()-1;j++)
		{
		 if(example.charAt(j)=='p')
		  count1++;
		}
		System.out.println("The number of occurrance of character p is:"+count1);
	}
}
